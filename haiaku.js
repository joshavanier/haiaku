/**
 * 俳悪
 * As requested by Kyr
 *
 * @author Josh Avanier
 * @license MIT
 */

'use strict';

const a = ' ! " # $ % & \' ( ) * + , - . / 0 1 2 3 4 5 6 7 8 9 : ; < = > ? @ A B C D E F G H I J K L M N O P Q R S T U V W X Y Z [ ] \\ ^ _ ` a b c d e f g h i j k l m n o p q r s t u v w x y z { | } ~'.split(' ');

const f = ' \uff01 \uff02 \uff03 \uff04 \uff05 \uff06 \uff07 \uff08 \uff09 \uff0a \uff0b \uff0c \uff0d \uff0e \uff0f \uff10 \uff11 \uff12 \uff13 \uff14 \uff15 \uff16 \uff17 \uff18 \uff19 \uff1a \uff1b \uff1c \uff1d \uff1e \uff1f \uff20 \uff21 \uff22 \uff23 \uff24 \uff25 \uff26 \uff27 \uff28 \uff29 \uff2a \uff2b \uff2c \uff2d \uff2e \uff2f \uff30 \uff31 \uff32 \uff33 \uff34 \uff35 \uff36 \uff37 \uff38 \uff39 \uff3a \uff3b \uff3c \uff3d \uff3e \uff3f \uff40 \uff41 \uff42 \uff43 \uff44 \uff45 \uff46 \uff47 \uff48 \uff49 \uff4a \uff4b \uff4c \uff4d \uff4e \uff4f \uff50 \uff51 \uff52 \uff53 \uff54 \uff55 \uff56 \uff57 \uff58 \uff59 \uff5a \uff5b \uff5c \uff5d \uff5e'.split(' ');

function translate (c) {
  const i = a.indexOf(c);
  return !!~i ? f[i] : c;
}

function process () {
  const n = i.value.split('\n');
  let r = '';
  let l = 0;
  let x = [];

  for (let i = 0, nl = n.length; i < nl; i++) {
    const il = n[i].length;
    il > l && (l = il);
    x[x.length] = n[i].split('');
  }

  for (let e = 0; e < l; e++) {
    for (let i = x.length - 1; i >= 0; i--) {
      r += !n[i][e] || n[i][e] === ' ' ?
        '\u3000' : translate(n[i].charAt(e));
    }
    r += '<br>';
  }

  return r;
}

function select (e) {
  let r;
  if (document.createTextRange) {
    r = document.createTextRange();
    r.moveToElementText(e);
    r.select();
  } else if (window.getSelection) {
    const s = window.getSelection();
    r = document.createRange();
    r.selectNodeContents(e);
    s.removeAllRanges();
    s.addRange(r);
  }
}

function print () {
  o.innerHTML = process();
  select(o);
}

c.onclick = _ => print();

print();

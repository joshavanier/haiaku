[![Haiaku](screenshot.png)](https://joshavanier.github.io/haiaku)

**Haiaku** is a text converter that rewrites text into top-down, right-to-left fullwidth format, as requested by a friend

```
水蛙古
の飛池
音びや
　込　
　む　

- 芭蕉
```

---

**[Josh Avanier](https://joshavanier.github.io)**

**[MIT](LICENSE)**
